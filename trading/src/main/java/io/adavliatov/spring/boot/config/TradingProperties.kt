package io.adavliatov.spring.boot.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull


@Configuration
@ConfigurationProperties(prefix = "trading")
@Validated
class TradingProperties {
    @NotEmpty
    lateinit var welcomeMessage: String
}