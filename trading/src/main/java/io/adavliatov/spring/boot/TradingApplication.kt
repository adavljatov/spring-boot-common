package io.adavliatov.spring.boot

import io.adavliatov.spring.boot.config.TradingConfig
import io.adavliatov.spring.boot.config.TradingProperties
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties


@RestController
@SpringBootApplication
class TradingApplication(@Autowired private val config: TradingProperties) {

    @RequestMapping("/")
    fun home(): String {
        return "port: ${config.welcomeMessage}!"
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(TradingApplication::class.java, *args)
        }
    }

}